package com.bensach.saul;

import com.badlogic.gdx.Game;
import com.bensach.saul.screens.GameScreen;

public class Occidere extends Game {

	public GameScreen gameScreen;

	@Override
	public void create() {
		gameScreen = new GameScreen(this);
		setScreen(gameScreen);
	}
}
