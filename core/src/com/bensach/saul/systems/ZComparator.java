package com.bensach.saul.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.bensach.saul.Mappers;
import com.bensach.saul.components.TransformComponent;

import java.util.Comparator;

public class ZComparator implements Comparator<Entity> {
    @Override
    public int compare(Entity o1, Entity o2) {
        float az = Mappers.transform.get(o1).position.z;
        float bz = Mappers.transform.get(o2).position.z;
        int res = 0;
        if(az > bz){
            res = 1;
        }else if(az < bz){
            res = -1;
        }
        return res;
    }
}
