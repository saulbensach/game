package com.bensach.saul.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.bensach.saul.Mappers;
import com.bensach.saul.components.PlayerComponent;
import com.bensach.saul.components.TransformComponent;
import com.bensach.saul.controllers.KeyboardController;

public class PlayerControlSystem extends IteratingSystem {

    private KeyboardController keyboardController;
    private float movementSpeed = 0.04f;

    public PlayerControlSystem(KeyboardController keyboardController) {
        super(Family.all(PlayerComponent.class).get());
        this.keyboardController = keyboardController;
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        TransformComponent transform = Mappers.transform.get(entity);

        if(keyboardController.right) {
            transform.position.x += movementSpeed;
        }
        if(keyboardController.left){
            transform.position.x += -movementSpeed;
        }
        if(keyboardController.up){
            transform.position.y += movementSpeed;
        }
        if(keyboardController.down){
            transform.position.y += -movementSpeed;
        }
    }
}
