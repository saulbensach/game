package com.bensach.saul.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.SortedIteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.bensach.saul.Mappers;
import com.bensach.saul.components.PlayerComponent;
import com.bensach.saul.components.TextureComponent;
import com.bensach.saul.components.TransformComponent;
import java.util.Comparator;
import java.util.Map;

public class RenderSystem extends SortedIteratingSystem {

    //TODO cambiar PPM al tiledmap que toca de 32
    static final float PPM = 16.0f;
    static final float FRUSTRUM_WIDTH = Gdx.graphics.getWidth() / PPM;
    static final float FRUSTRUM_HEIGHT = Gdx.graphics.getHeight() / PPM;
    public static final float PIXELS_TO_METERS = 1.0f / PPM;
    private static Vector2 metersDimensions = new Vector2();

    private static final Vector2 pixelDimensions = new Vector2();
    public static Vector2 getScreenSizeInMeters(){
        metersDimensions.set(Gdx.graphics.getWidth() * PIXELS_TO_METERS, Gdx.graphics.getHeight() * PIXELS_TO_METERS);
        return metersDimensions;
    }

    public static Vector2 getScreenSizeInPixesl(){
        pixelDimensions.set(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        return pixelDimensions;
    }

    public static float pixelToMeters(float pixelValue) {
        return pixelValue * PIXELS_TO_METERS;
    }

    private SpriteBatch batch;
    private Array<Entity> renderQueue;
    private Comparator<Entity> comparator;
    private OrthographicCamera camera;

    public RenderSystem(SpriteBatch batch) {
        super(Family.all(TransformComponent.class, TextureComponent.class).get(), new ZComparator());
        comparator = new ZComparator();
        renderQueue = new Array<Entity>();
        this.batch = batch;
        camera = new OrthographicCamera(FRUSTRUM_WIDTH, FRUSTRUM_HEIGHT);
        camera.zoom = 0.5f;
        camera.position.set(FRUSTRUM_WIDTH / 2f, FRUSTRUM_HEIGHT / 2f, 0);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        renderQueue.add(entity);
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        renderQueue.sort(comparator);

        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.enableBlending();

        batch.begin();
        for(Entity entity: renderQueue) {

            PlayerComponent playerComponent = Mappers.player.get(entity);

            TextureComponent textureComponent = Mappers.texture.get(entity);
            TransformComponent transformComponent = Mappers.transform.get(entity);

            if(textureComponent.region == null || transformComponent.isHidden){
                continue;
            }

            if(playerComponent != null) {
                camera.position.set(transformComponent.position);
                camera.update();
            }

            float width = textureComponent.region.getRegionWidth();
            float height = textureComponent.region.getRegionWidth();
            float originX = width / 2;
            float originY = height / 2;

            batch.draw(
                    textureComponent.region,
                    transformComponent.position.x - originX,
                    transformComponent.position.y - originY,
                    originX,
                    originY,
                    width,
                    height,
                    pixelToMeters(transformComponent.scale.x),
                    pixelToMeters(transformComponent.scale.y),
                    transformComponent.rotation
            );

        }
        batch.end();
        renderQueue.clear();
    }
}
