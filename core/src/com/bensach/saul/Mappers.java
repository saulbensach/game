package com.bensach.saul;

import com.badlogic.ashley.core.ComponentMapper;
import com.bensach.saul.components.PlayerComponent;
import com.bensach.saul.components.TextureComponent;
import com.bensach.saul.components.TransformComponent;

public class Mappers {
    public static final ComponentMapper<TransformComponent> transform = ComponentMapper.getFor(TransformComponent.class);
    public static final ComponentMapper<TextureComponent> texture = ComponentMapper.getFor(TextureComponent.class);
    public static final ComponentMapper<PlayerComponent> player = ComponentMapper.getFor(PlayerComponent.class);
}
