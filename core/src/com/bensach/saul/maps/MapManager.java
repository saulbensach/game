package com.bensach.saul.maps;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.bensach.saul.components.TextureComponent;
import com.bensach.saul.components.TransformComponent;

public class MapManager {

    public static void create(String name, PooledEngine engine) {
        TiledMap map = new TmxMapLoader().load(name);
        for(int i = 0 ; i < map.getLayers().getCount(); i++){
            TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(i);
            layer.setVisible(true);
            for(int x = 0; x < layer.getWidth(); x++){
                for(int y = 0; y < layer.getHeight(); y++){
                    TiledMapTileLayer.Cell cell = layer.getCell(x, y);
                    //if(cell == null && cell.getTile() == null) continue;
                    Entity entity = engine.createEntity();
                    TransformComponent transformComponent = new TransformComponent();
                    transformComponent.position.set(x,y, 0.4f);
                    TextureComponent textureComponent = new TextureComponent();
                    textureComponent.region = cell.getTile().getTextureRegion();
                    entity.add(transformComponent);
                    entity.add(textureComponent);
                    engine.addEntity(entity);
                }
            }
        }
    }

}
