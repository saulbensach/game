package com.bensach.saul.screens;

import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.bensach.saul.Occidere;
import com.bensach.saul.controllers.KeyboardController;
import com.bensach.saul.systems.PlayerControlSystem;
import com.bensach.saul.systems.RenderSystem;

public abstract class BaseScreen implements Screen {

    protected RenderSystem renderSystem;
    protected PlayerControlSystem playerControlSystem;
    protected SpriteBatch batch;
    protected Occidere occidere;
    protected PooledEngine engine;
    private TiledMap map;

    public BaseScreen(Occidere occidere) {
        this.occidere = occidere;
        batch = new SpriteBatch();
        KeyboardController keyboardController = new KeyboardController();
        Gdx.input.setInputProcessor(keyboardController);
        playerControlSystem = new PlayerControlSystem(keyboardController);
        engine = new PooledEngine();
        renderSystem = new RenderSystem(batch);
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        engine.update(delta);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
