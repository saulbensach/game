package com.bensach.saul.screens;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.bensach.saul.Occidere;
import com.bensach.saul.components.PlayerComponent;
import com.bensach.saul.components.TextureComponent;
import com.bensach.saul.components.TransformComponent;
import com.bensach.saul.maps.MapManager;


public class GameScreen extends BaseScreen {

    public GameScreen(Occidere occidere) {
        super(occidere);
        engine.addSystem(renderSystem);
        engine.addSystem(playerControlSystem);
        MapManager.create("mapa.tmx", engine);
        createPlayer();
    }

    private void createPlayer() {
        TextureComponent textureComponent = new TextureComponent();
        textureComponent.region = new TextureAtlas.AtlasRegion(new Texture("player.png"), 0,0, 16, 16);
        TransformComponent transformComponent = new TransformComponent();
        transformComponent.position.set(0,0, 0.5f);
        Entity entity = engine.createEntity();
        entity.add(new PlayerComponent());
        entity.add(transformComponent);
        entity.add(textureComponent);
        engine.addEntity(entity);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
    }
}
