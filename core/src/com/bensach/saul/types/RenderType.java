package com.bensach.saul.types;

public enum RenderType {

    CIRCLE, LINE, RECTANGLE, POLYGON, TEXT

}
