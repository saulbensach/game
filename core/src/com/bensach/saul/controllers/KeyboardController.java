package com.bensach.saul.controllers;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;

public class KeyboardController implements InputProcessor {

    public boolean left, right, up, down;

    public KeyboardController () {
        left = false; right = false;
        up = false; down = false;
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode){
            case Input.Keys.W : {
                up = true;
                break;
            }
            case Input.Keys.S : {
                down = true;
                break;
            }
            case Input.Keys.A : {
                left = true;
                break;
            }
            case Input.Keys.D : {
                right = true;
                break;
            }
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch (keycode){
            case Input.Keys.W : {
                up = false;
                break;
            }
            case Input.Keys.S : {
                down = false;
                break;
            }
            case Input.Keys.A : {
                left = false;
                break;
            }
            case Input.Keys.D : {
                right = false;
                break;
            }
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
